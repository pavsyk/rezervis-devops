A Docker image for Rezervis web application - production version.

The image containes slimmed WildFly (i.e. without deployment scanner, web console etc.)

Notes:

* File "rezervis.war" must be deployed via jboss-cli after docker-compose up.
* This image must not be published in the Docker Hub.
* A container needs a h2db container and network - see docker compose.

Build:
	`docker build -t pavsyk/rezervis-prod .`

Run via docker compose - see compose-prod directory.
