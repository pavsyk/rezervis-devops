A Docker image for H2 database (http://h2database.com).

Build:
	`docker build -t pavsyk/h2database .`

Attention: h2database.sh must not have DOS line endings. Use `dos2unix h2database.sh`, if needed. Also set the LF line ending for this file in the repository.

Run (example for DockerTools for Windows):
	`docker run -d --name h2db -p 8082:8082 -p 9092:9092 -v /Users/pavel/db/data:/h2-data pavsyk/h2database`

Run (example for Docker for Windows):
	`docker run -d --name h2db -p 8082:8082 -p 9092:9092 -v c:/Users/pavel/db/data:/h2-data pavsyk/h2database`

Note: Also /h2-backup volume should be mapped to the host.