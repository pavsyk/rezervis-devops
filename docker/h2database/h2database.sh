#!/bin/sh
exec java -Xms128M -Xmx384M -cp "/opt/h2/bin/*" org.h2.tools.Server \
 	-web -webAllowOthers -webPort 8082 \
 	-tcp -tcpAllowOthers -tcpPort 9092 \
 	-baseDir $DATA_DIR