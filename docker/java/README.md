A Docker image based on phusion/baseimage with JDK 8, EPEL repo etc. The purpose of the image 
is to be a base for derived images which need Java.

Build:
	`docker build -t pavsyk/java .`