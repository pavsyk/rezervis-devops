A Docker Compose configuration for running web app and database containers.

Before running, copy "h2.env.template" to "h2.env" and set the real username and password of the database.
The same for "mail.env.template".

Run:
	`docker-compose up -d`
