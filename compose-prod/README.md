A Docker Compose configuration for running web app and database containers.

Before running, copy "h2.env.template" to "h2.env" and set the real username and password of the database.
The same for "mail.env.template".

Run:
	`docker-compose up -d`

Deploy / redeploy rezervis.war from the Docker host e.g.: 
  ./jboss-cli.sh -c -u=admin -p=Welcome1  --command="deploy --force rezervis.war"
or on Windows:
  jboss-cli.bat --controller=10.0.75.1:9990 -c -u=admin -p=Welcome1  --command="deploy --force rezervis.war"
